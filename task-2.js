// Write two ways of creating objects using functions that represent
// the items of the products on the products.jpg on the source directory

// Factory Function
// You code here ...
function createMeal(name, info, cost) {
    return {
        name,
        info,
        cost 
    };
}
    const popular = createMeal('Popular','meat, potato, garlik, lavash','$4');
    const retro = createMeal('Retro','meat, potato, garlik, lavash','$3');
    const tradition = createMeal('Tradition','meat, potato, garlik, lavash','$5');
    const trend = createMeal('Trend','meat, potato, garlik, lavash','$3.5');

    console.log(popular);
    console.log(retro);
    console.log(tradition);
    console.log(trend);


// Factore Function
function FastFood(name, info, cost) {
    this.name = name,
    this.info = info,
    this.cost = cost
}

const clubSandwich = new FastFood ('clubSandwich', 'potato, chicken, tomato, sauce', '$4.99');
const chickenWings = new FastFood ('chickenWings', 'salad, chicken, tomato, sauce', '$5.99');
const nagets       = new FastFood ('nagets', 'potato, chicken, tomato, sauce', '$6.99');
const bigBeef      = new FastFood ('bigBeef', 'potato, chicken, tomato, sauce', '$3.99');


console.log(clubSandwich);
console.log(chickenWings);
console.log(nagets);
console.log(bigBeef);

